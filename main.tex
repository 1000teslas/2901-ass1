\documentclass[answers]{exam}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{xcolor}
\usepackage{booktabs}
\usepackage{pdfpages}

\title{MATH2901 Assignment 1}
\author{William Ha \and Brendan Nguyen \and Kevin Tran}
\date{March 2018}

\begin{document}
\begin{titlepage}
	\centering
	\includegraphics[width=0.5\textwidth]{unsw}\par\vspace{1cm}
	{\scshape\LARGE MATH2901: Higher Theory of Statistics \par}
	\vspace{1cm}
	{\scshape\Large Assignment One\par}
	\vspace{2cm}
	{\Large\itshape William Ha\par}
	{\Large\itshape Brendan Nguyen\par}
	{\Large\itshape Kevin Tran\par}

	\vfill

% Bottom of the page
	{\large \today\par}
\end{titlepage}

\begin{questions}

\question Let $A$ be a subset of $\Omega$
\begin{parts}

\part Let event $A$ be independent of itself. Show that $P(A)$ is either 1 or 0.

\begin{solution}
If $A$ is independent of itself, then by definition we have
\begin{equation*}
    P(A \cap A) = P(A) P(A)
\end{equation*}
Since $A \cap A = A$ then the equation becomes
\begin{equation*}
    P(A) = [P(A)]^2
\end{equation*}
Rearranging the terms and factorising,
\begin{equation*}
    P(A)[1 - P(A)] = 0 \
\end{equation*}
Upon solving, we have that $P(A)$ is either $1$ or $0$ and so the proof is complete.
\end{solution}

\part Let event $A$ be such that $P(A) = 1$ or $P(A) = 0$. Show that $A$ and an arbitrary event $B$ are independent.

\begin{solution}
Consider the two cases where $P(A) = 0$ or $P(A) = 1$. 
\\
\\
When $P(A) = 0$, we note that $(A \cap B) \subseteq A$. Due to the monotonicity of probability, 
\begin{equation*}
    P(A \cap B) \leq P(A) = 0
\end{equation*}
Now according to the first axiom of probability, $P(A \cap B) \geq 0$ so using the above result it follows that $P(A \cap B) = 0$. As a result, the equation
\begin{equation*}
    P(A \cap B) = P(A) P(B)
\end{equation*}
is satisfied and by definition $A$ and $B$ are independent events.
\\
\\
When P(A) = 1, we note that $B = (B \cap A^c) \cup (B \cap A)$, which is a union of disjoint sets. So using the additivity of probability, 
\begin{equation}
    P(B) = P(B \cap A^c) + P(B \cap A)
\end{equation}
But using Bayes' Theorem
\begin{equation*}
    P(B \cap A^c) = P(B | A^c) P(A^c) = 0
\end{equation*}
so (1) becomes
\begin{equation*}
    P(A \cap B) = P(B) = P(A) P(B)
\end{equation*}
and hence $A$ and $B$ are independent events.
\end{solution}

\end{parts}

\newpage

\question A gambler has in his pocket a fair coin (with a head and a tail side) and a two headed coin.

\begin{parts}
\part He selects one of the coins at random, and when he flips it, it shows heads. What is the probability that it is the fair coin?

% Set the overall layout of the tree
\tikzstyle{level 1}=[level distance=3.5cm, sibling distance=3.5cm]
\tikzstyle{level 2}=[level distance=3.5cm, sibling distance=2cm]

% Define styles for bags and leafs
\tikzstyle{bag} = [text width=4em, text centered]
\tikzstyle{end} = [circle, minimum width=3pt,fill, inner sep=0pt]

% The sloped option gives rotated edge labels. Personally
% I find sloped labels a bit difficult to read. Remove the sloped options
% to get horizontal labels. 

\begin{solution}
Let F be the event of a Fair coin, let H be the event of landing on Heads and T be the event of landing on Tails.

\begin{tikzpicture}[grow=right, sloped]
\node[bag] {}
    child {
        node[bag] {Fair H, T}        
            child {
                node[end, label=right:] {}
                edge from parent
                node[above] {$H$}
                node[below]  {$\frac{1}{2}$}
            }
            child {
                node[end, label=right:] {}
                edge from parent
                node[above] {$T$}
                node[below]  {$\frac{1}{2}$}
            }
            edge from parent 
            node[below]  {$\frac{1}{2}$}
    }
    child {
        node[bag] {Bias H, H}        
            child {
                node[end, label=right:] {}
                edge from parent
                node[above] {H}
                node[below]  {$1$}
            }
        edge from parent         
            node[below]  {$\frac{1}{2}$}
    };
\end{tikzpicture}

From Bayes' Theorem
\begin{align*}
    P(F | H) &= \frac{P(H | F)P(F)}{P(H)} \\
             &= \frac{\frac{1}{2}\times\frac{1}{2}}{\frac{1}{2}(1)+(\frac{1}{2})^2} \\
             &= \frac{1}{3} \\
\end{align*}
        
\end{solution}



\part Suppose that he flips the same coin a second time and again it shows heads. Now what is the probability that it is the fair coin?

\begin{solution}
Let $H_2$ be the event of landing on Heads twice
\begin{align*}
    P(F | H_2) &= \frac{P(H_2 | F)P(F)}{P(H_2)} \\
              &= \frac{(\frac{1}{2})^2\times\frac{1}{2}}{\frac{1}{2}(1^2)+(\frac{1}{2})^3} \\
              &= \frac{1}{5} \\
\end{align*}

\end{solution}

\newpage

\part Suppose that he flips the same coin a third time and it shows tails. Now what is the probability that it is the fair coin?


\begin{solution}
Let $\mathrm{H}_2$T be the event of landing on Heads twice and then on Tails(in that order)
\begin{align*}
    P(F | H_2T) &= \frac{P(H_2T | F)P(F)}{P(H_2T)} \\
              &= \frac{(\frac{1}{2})^3\times\frac{1}{2}}{\frac{1}{2}(1^2)(0)+(\frac{1}{2})^4} \\
              &= 1 
\end{align*}
Note that since the biased coin doesn't even have tails, it must be the fair coin and so the use of Bayes' Theorem is unnecessary.

\end{solution}
\end{parts}

\question Bill and George go target shooting together. Both shoot at a target at the same time. Suppose Bill hits the target with probability 0.7, whereas George, independently, hits the target with probability 0.4
\begin{parts}
\part Given that exactly one shot hit the target, what is the probability that it was George’s shot.
\begin{solution}
Let G be the event of George hitting the target, B be the event of Bill hitting the target, $\mathrm{H}_{1}$ being the target getting shot once
\begin{align*}
    P(G | H_{1}) &= \frac{P(H_{1} | G)P(G)}{P(H_{1})} \\
              &= \frac{P(B^c)P(G)}{P(B^c)P(G)+ P(G^c)P(B)} \\
              &= \frac{0.3\times0.4}{0.3\times0.4+ 0.6\times0.7} \\
              &= \frac{2}{9} \\
\end{align*}


\end{solution}

\part Given that the target is hit, what is the probability that George hit it.
\begin{solution}
Let H be the event the target is hit
\begin{align*}
    P(G | H)  &= \frac{\overbrace{P(H | G)}^{1}P(G)}{P(H)} \\
              &= \frac{1(0.4)}{1 - P(B^c\cap G^c)} \\
              &= \frac{1(0.4)}{1 - P(B^c)P(G^c)}  && \text{as $B^c$ and $G^c$ are independent events}\\
              &= \frac{0.4}{1 - (0.3)(0.6)} \\
              &= \frac{20}{41} \\
\end{align*}
\end{solution}

\end{parts}

\newpage

\question Give an example of two sets $A$ and $B$ (subsets of sample space $\Omega$) such that
\begin{align*}
    P(B|A) + P(B|A^c) &= 1 \\
    P(B|A) + P(B^c|A^c) &= 1
\end{align*}
    
\begin{solution}

\begin{center}
\begin{tikzpicture}
\draw (0,0) -- (6,0) -- (6,4) -- (0,4) -- (0,0);
\draw (3,0) -- (3,4);
\fill[gray!15!white, draw = black] (3,0) rectangle (6,4) node[anchor=north east] {\color{black} A};
\fill[gray!40!white, draw = black] (3,2) rectangle (0,0) node[anchor=south west] {\color{black} B};
\fill[gray!27.5!white, draw = black] (3,2) rectangle (6,0) node[anchor=south east] {\color{black} $A \cap B$};
\draw (0,2) -- (6,2);
\end{tikzpicture}    
\end{center}

Consider a random experiment with a discrete sample space and equally likely outcomes. If $A$ and $B$ are as shown in the diagram, then $P(B|A) = \frac{1}{2}$ since there are half as many elements in $A \cap B$ than in $A$. Similarly, we have $P(B|A^c) = P(B^c|A^c) = \frac{1}{2}$. Upon substituting these, the equations are satisfied. 

To provide an example of a real experiment, consider a 3 coin toss. Let $A = $\{HHH, HHT, HTH, THH\} and $B = $\{HTH, THH, HTT, THT\} so $A \cap B =$ \{HTH, THH\}.
\end{solution}

\question Let X be a random variable with density function:
\begin{equation*}
    f_X (x) = \frac{2}{x^2}
\end{equation*}
for $1 < x < 2$ and $f_X (x) = 0$ otherwise.

\begin{parts}
\part Determine the cumulative distribution function of X
\begin{solution}
\begin{align*}
    F_X (x) &= \int_1^{x} f_X(x)\,\mathrm{d}x \\
    &= \int_1^{x} \frac{2}{x^2}\,\mathrm{d}x  \\
    &= 2 - \frac{2}{x} \qquad \text{where} \quad 1 < x < 2
\end{align*}
\end{solution}


\part Calculate the median and expected value of X
\begin{solution}
The median M is the value of X for which $P(X \leq M) = 0.5$. This is equivalent to $F_X (M) = 0.5.$ Solving this,
\begin{align*}
    2 - \frac{2}{M} &= \frac{1}{2} \implies M = \frac{4}{3}
\end{align*}
\
The expected value of X is given by
\begin{align*}
    \mathrm{E}[X] &= \int_1^2 xf_X(x)\,\mathrm{d}x \\
    &= \int_1^2 \frac{2}{x}\, \mathrm{d}x \\
    &= 2\ln(x) \bigg\rvert_{1}^2 \\
    &= 2\ln(2)
\end{align*}
\end{solution}
\end{parts}

\question Let $X \sim $ Binomial$(n, p).$ By applying the binomial expansion to $\left( (1-p) - p \right)^n$, show
\begin{equation*}
    (1-2p)^n = P(X\;\text{is even}) - P(X\;\text{is odd})
\end{equation*}
hence or otherwise deduce that $P(X\;\text{is even}) = \frac{1}{2} \left( 1 + (1-2p)^n \right)$

\begin{solution}
Note that
\begin{align*}
    (1-2p)^n = ((1-p)-p)^n &= \sum_{k=0}^n\binom{n}{k}(1-p)^{n-k}(-p)^{k} \\
                           &= \sum_{k=0}^n\binom{n}{k}(1-p)^{n-k}p^k(-1)^k \\
                           &= \sum_{0\leq k \leq n\;\text{even}} \binom{n}{k}(1-p)^{n-k}p^k \quad - \sum_{0\leq k \leq n\;\text{odd}} \underbrace{\binom{n}{k}(1-p)^{n-k}p^k}_\text{$P(X=k)$} \\
                           &= P(X\;\text{is even}) - P(X\;\text{is odd}) \\
    \intertext{Also, since the events `X is even' and `X is odd' partition the sample space,}
    1 &= P(X\;\text{is even}) + P(X\;\text{is odd}). 
    \intertext{Hence}
    1+(1-2p)^n &= P(X\;\text{is even}) - P(X\;\text{is odd}) + P(X\;\text{is even}) + P(X\;\text{is odd}) \\
                           &= 2P(X\;\text{is even}) \\
    \intertext{and so}
    P(X\;\text{is even}) &= \frac{1+(1-2p)^n}{2}.\\
\end{align*}
\end{solution}

\question R Question

\begin{enumerate}
    \item There are 799 observations.

    \item 
       see Table 1
       \begin{table}[p]
         \caption{logNervedata}
         \begin{tabular}{lll}
           \toprule
           Min. & 0.50 \\
           1st Qu. & 3.50 \\
           Median & 7.50 \\
           Mean & 10.95 \\
           3rd Qu. & 15.00 \\
           Max. & 69.00 \\
           \bottomrule
         \end{tabular}
       \end{table}
     \item The data in Nervedata is located mostly at 5 to 15. The distribution
       is right-skewed. There is medium spread. There are outliers.
     \item
       See Table 2. The data in logNervedata is located mostly at 1.5 to 2.5. The distribution is mostly
       symmetric. There is low spread. There are no outliers.
       \begin{table}[p]
         \caption{logNervedata}
         \begin{tabular}{lll}
           \toprule
           Min.   & -0.6931 \\
           1st Qu.& 1.2528 \\
           Median & 2.0149 \\
           Mean   & 1.9113 \\
           3rd Qu.& 2.7081 \\
           Max.   & 4.2341 \\
           \bottomrule
         \end{tabular}
       \end{table}
     \item
       See Table 3. The data in rootNervedata is located mostly at 2 to 4. The distribution is right-skewed.
       There is medium spread. There are outliers.
       \begin{table}[p]
         \caption{rootNervedata}
         \begin{tabular}{lll}
           \toprule
           Min.   &0.7071  \\
           1st Qu.&1.8708  \\
           Median &2.7386  \\
           Mean   &2.9694  \\
           3rd Qu.&3.8730  \\
           Max.   &8.3066  \\
           \bottomrule
         \end{tabular}
       \end{table}
     \item See Table 4. The data in squareNervedata is located mostly small. The
       distribution is right-skewed. The data is highly spread. There are many
       outliers.
       \begin{table}[p]
         \caption{squareNervedata}
         \begin{tabular}{lll}
           \toprule
           Min.   &   0.25  \\
           1st Qu.&  12.25  \\
           Median &  56.25  \\
           Mean   & 229.19  \\
           3rd Qu.& 225.00  \\
           Max.   &4761.00  \\
           \bottomrule
         \end{tabular}
       \end{table}
     \item
         See Table 5. The data in atanNervedata is near 1.4. The distribution is
         left-skewed. The data is not spread. There are few outliers.

         See Table 6. The data in tanhNervedata is near 1. The distribution is left-skewed.
         The data is not spread. There are some outliers.
       \begin{table}[p]
         \caption{atanNervedata}
         \begin{tabular}{lll}
           \toprule
           Min.   &0.4636  \\
           1st Qu.&1.2925  \\
           Median &1.4382  \\
           Mean   &1.3392  \\
           3rd Qu.&1.5042  \\
           Max.   &1.5563  \\
           \bottomrule
         \end{tabular}
         \caption{tanhNervedata}
         \begin{tabular}{lll}
           \toprule
           Min.   &0.4621  \\
           1st Qu.&0.9982  \\
           Median &1.0000  \\
           Mean   &0.9626  \\
           3rd Qu.&1.0000  \\
           Max.   &1.0000  \\
           \bottomrule
         \end{tabular}
       \end{table}
     \item
       The log transformation produced the most symmetric transformed data.
\end{enumerate}
\end{questions}

\begin{figure}
\includegraphics[scale=0.45]{plots}
\centering
\end{figure}
\end{document}
